Dans la marine on ne fait pas grand chose, mais on le fait de bonne heure (Marin Shadok).
